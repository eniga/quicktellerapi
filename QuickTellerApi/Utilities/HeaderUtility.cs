﻿using System;
using System.Text;
using Microsoft.Extensions.Configuration;
using QuickTellerApi.Models;

namespace QuickTellerApi.Utilities
{
    public static class HeaderUtility
    {
        public static APIHeader GenerateAPIHeader(string http_verb, string url, string encryptionType, IConfiguration configuration)
        {
            string ClientID = configuration.GetValue<String>("QuickTeller:ClientID");
            string Secret = configuration.GetValue<String>("QuickTeller:Secret");
            string RequestReferencePrefix = configuration.GetValue<String>("QuickTeller:RequestReferencePrefix");
            string TerminalID = configuration.GetValue<String>("QuickTeller:TerminalID");
            string Nonce = Guid.NewGuid().ToString().Replace("-", "") + Helper.GenerateRandomNumber(10);
            var timestamp = DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds.ToString().Substring(0, 10);

            string SignatureInput = http_verb + "&" + Uri.EscapeDataString(url) + "&" + timestamp + "&" + Nonce + "&" + ClientID + "&" + Secret;
            string encryptedSignatureInput = string.Empty;
            switch (encryptionType.ToUpper())
            {
                case "SHA1":
                    encryptedSignatureInput = SHAUtility.SHA1Hash(SignatureInput);
                    break;
                case "SHA256":
                    encryptedSignatureInput = SHAUtility.SHA256Hash(SignatureInput);
                    break;
                case "SHA512":
                    encryptedSignatureInput = SHAUtility.SHA512Hash(SignatureInput);
                    break;
            }

            APIHeader result = new APIHeader()
            {
                Authorization = Helper.Base64Encode(ClientID),
                Signature = encryptedSignatureInput,
                SignatureMethod = encryptionType.ToUpper(),
                Timestamp = timestamp,
                Nonce = Nonce,
                ClientID = ClientID,
                RequestReferencePrefix = RequestReferencePrefix,
                Secret = Secret,
                TerminalID = TerminalID
            };
            return result;
        }

    }
}
