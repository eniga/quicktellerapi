﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QuickTellerApi.Models
{
    public class SendBillPaymentNotificationPayload
    {
        public string AccountNumber { get; set; }
        public string PaymentCode { get; set; }
        public string CustomerId { get; set; }
        public decimal Amount { get; set; }
        public string requestReference { get; set; }
    }

    public class SendBillPaymentNotificationRequest
    {
        public string TerminalId { get; set; }
        public string paymentCode { get; set; }
        public string customerId { get; set; }
        public string customerMobile { get; set; }
        public string customerEmail { get; set; }
        public string amount { get; set; }
        [Key]
        public string requestReference { get; set; }
    }

    [Table("tbl_qt_billpayments")]
    public class SendBillPaymentDb : BillPaymentNotificationResponse
    {
        [Key]
        public string requestReference { get; set; }
        public string TerminalId { get; set; }
        public string paymentCode { get; set; }
        public string customerId { get; set; }
        public string customerMobile { get; set; }
        public string customerEmail { get; set; }
        public string amount { get; set; }
        public string accountnumber { get; set; }
    }
}
