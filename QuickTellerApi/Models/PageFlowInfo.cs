﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MicroOrm.Dapper.Repositories.Attributes;

namespace QuickTellerApi.Models
{
    [Table("tbl_qt_pageflowinfo")]
    public class PageFlowInfo
    {
        [Key, Identity]
        public string billerid { get; set; }
        public bool allowRetry { get; set; }
        public string finishButtonName { get; set; }
        public bool performInquiry { get; set; }
        public string startPage { get; set; }
        public bool usesPaymentItems { get; set; }
    }
}
