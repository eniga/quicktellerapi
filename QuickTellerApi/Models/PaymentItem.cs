﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QuickTellerApi.Models
{
    [Table("tbl_qt_paymentitems")]
    public class PaymentItem
    {
        public string categoryid { get; set; }
        public string billerid { get; set; }
        public bool isAmountFixed { get; set; }
        [Key, Required]
        public string paymentitemid { get; set; }
        public string paymentitemname { get; set; }
        public string amount { get; set; }
        public string code { get; set; }
        public string currencyCode { get; set; }
        public string currencySymbol { get; set; }
        public string itemCurrencySymbol { get; set; }
        public string sortOrder { get; set; }
        public string pictureId { get; set; }
        public string paymentCode { get; set; }
        public string itemFee { get; set; }
    }
}
