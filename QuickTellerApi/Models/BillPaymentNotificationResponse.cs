﻿using System;
namespace QuickTellerApi.Models
{
    public class BillPaymentNotificationResponse
    {
        public string responseCode { get; set; }
        public string responseMessage { get; set; }
        public string responseCodeGrouping { get; set; }
        public string transactionRef { get; set; }
    }
}
