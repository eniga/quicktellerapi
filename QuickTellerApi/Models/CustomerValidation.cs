﻿using System;
using System.Collections.Generic;

namespace QuickTellerApi.Models
{
    public class CustomerValidationRequest
    {
        public IEnumerable<CustomerValidationPayload> customers { get; set; }
    }

    public class CustomerValidationPayload
    {
        public string customerId { get; set; }
        public string paymentCode { get; set; }
    }

    public class CustomerValidationResponse
    {
        public IEnumerable<CustomerValidationResponsePayload> Customers { get; set; }
    }

    public class CustomerValidationResponsePayload
    {
        public string paymentCode { get; set; }
        public string customerId { get; set; }
        public string responseCode { get; set; }
        public string responseDescription { get; set; }
        public string amount { get; set; }
        public string amountType { get; set; }
    }
}
