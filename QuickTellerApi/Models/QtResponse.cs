﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MicroOrm.Dapper.Repositories.Attributes;

namespace QuickTellerApi.Models
{
    [Table("tbl_qt_response")]
    public class QtResponse
    {
        [Key, Identity]
        public string Code { get; set; }
        public string Description { get; set; }
        public string Resolution { get; set; }
        public bool Status { get; set; }
    }
}
