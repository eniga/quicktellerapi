﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MicroOrm.Dapper.Repositories.Attributes;
using MicroOrm.Dapper.Repositories.Attributes.Joins;

namespace QuickTellerApi.Models
{
    [Table("tbl_qt_billers")]
    public class Biller
    {
        public string categoryid { get; set; }
        public string categoryname { get; set; }
        public string categorydescription { get; set; }
        [Key, Identity, ]
        public string billerid { get; set; }
        public string billername { get; set; }
        public string customerfield1 { get; set; }
        public string supportemail { get; set; }
        public string paydirectProductId { get; set; }
        public string paydirectInstitutionId { get; set; }
        public string narration { get; set; }
        public string shortName { get; set; }
        public string surcharge { get; set; }
        public string currencyCode { get; set; }
        public string quickTellerSiteUrlName { get; set; }
        public string smallImageId { get; set; }
        public string amountType { get; set; }
        [LeftJoin("tbl_qt_pageflowinfo", "billerid", "billerid")]
        public PageFlowInfo pageFlowInfo { get; set; }
        public string currencySymbol { get; set; }
        public string logoUrl { get; set; }
        public string networkId { get; set; }
        public string productCode { get; set; }
        public string type { get; set; }
    }
}
