﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MicroOrm.Dapper.Repositories.Attributes;

namespace QuickTellerApi.Models
{
    [Table("tbl_qt_categories")]
    public class Category
    {
        [Key, Identity]
        public string categoryid { get; set; }
        public string categoryname { get; set; }
        public string categorydescription { get; set; }
    }
}
