﻿using System;
using System.Collections.Generic;

namespace QuickTellerApi.Models
{
    public class ErrorResponse
    {
        public List<Error> errors { get; set; }
        public Error error { get; set; }
    }

    public class Error
    {
        public string code { get; set; }
        public string message { get; set; }
    }
}
