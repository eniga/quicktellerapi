﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using QuickTellerApi.Models;
using QuickTellerApi.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace QuickTellerApi.Controllers
{
    [Route("api/[controller]")]
    public class BillersController : Controller
    {
        BillerService service;

        public BillersController(IConfiguration configuration, ILogger<BillerService> log)
        {
            service = new BillerService(configuration, log);
        }

        // GET: api/values
        [HttpGet]
        public IEnumerable<Biller> List()
        {
            return service.List();
        }

        // GET api/values/5
        [HttpGet("{BillerId}")]
        public Biller Get(string BillerId)
        {
            return service.GetById(BillerId);
        }

        // GET: api/values
        [HttpGet("ByCategory/{CategoryId}")]
        public IEnumerable<Biller> GetByCategory(string CategoryId)
        {
            return service.GetByCategory(CategoryId);
        }
    }
}
