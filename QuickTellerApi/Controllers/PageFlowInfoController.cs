﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using QuickTellerApi.Models;
using QuickTellerApi.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace QuickTellerApi.Controllers
{
    [Route("api/[controller]")]
    public class PageFlowInfoController : Controller
    {
        PageFlowInfoService service;

        public PageFlowInfoController(IConfiguration configuration, ILogger<PageFlowInfoService> log)
        {
            service = new PageFlowInfoService(configuration, log);
        }

        // GET: api/values
        [HttpGet]
        public IEnumerable<PageFlowInfo> List()
        {
            return service.List();
        }

        // GET api/values/5
        [HttpGet("{BillerId}")]
        public PageFlowInfo Get(string BillerId)
        {
            return service.GetById(BillerId);
        }
    }
}
