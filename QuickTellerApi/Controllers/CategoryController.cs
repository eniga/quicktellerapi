﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using QuickTellerApi.Models;
using QuickTellerApi.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace QuickTellerApi.Controllers
{
    [Route("api/[controller]")]
    public class CategoryController : Controller
    {
        CategoryService service;

        public CategoryController(IConfiguration configuration, ILogger<CategoryService> log)
        {
            service = new CategoryService(configuration, log);
        }

        // GET: api/values
        [HttpGet]
        public IEnumerable<Category> List()
        {
            return service.List();
        }

        // GET api/values/5
        [HttpGet("{categoryid}")]
        public Category Get(string categoryid)
        {
            return service.GetById(categoryid);
        }
    }
}
