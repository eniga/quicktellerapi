﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using QuickTellerApi.Models;
using QuickTellerApi.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace QuickTellerApi.Controllers
{
    [Route("api/[controller]")]
    public class QrResponseController : Controller
    {
        QrResponseService service;

        public QrResponseController(IConfiguration configuration, ILogger<QrResponseService> log)
        {
            service = new QrResponseService(configuration, log);
        }

        // GET: api/values
        [HttpGet]
        public async Task<IEnumerable<QtResponse>> List()
        {
            return await service.ListAsync();
        }

        // GET api/values/5
        [HttpGet("{Code}")]
        public async Task<QtResponse> Get(string Code)
        {
            return await service.GetByIdAsync(Code);
        }
    }
}
