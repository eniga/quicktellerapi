﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using QuickTellerApi.Models;
using QuickTellerApi.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace QuickTellerApi.Controllers
{
    [Route("api/[controller]")]
    public class SendBillPaymentController : Controller
    {
        SendBillPaymentService service;

        public SendBillPaymentController(IConfiguration configuration, ILogger<QrResponseService> log)
        {
            service = new SendBillPaymentService(configuration, log);
        }

        // GET api/values/5
        [HttpGet("{RequestReference}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(BillPaymentNotificationResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorResponse))]
        public ActionResult QueryTransaction(string RequestReference)
        {
            var result = service.QueryTransaction(RequestReference);
            if (result.GetType() == typeof(BillPaymentNotificationResponse))
                return Ok(result);
            else
                return BadRequest(result);
        }

        // POST api/values
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(BillPaymentNotificationResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorResponse))]
        public ActionResult Post([FromBody]SendBillPaymentNotificationPayload req)
        {
            var result = service.SendBillPaymentTransaction(req);
            if (result.GetType() == typeof(BillPaymentNotificationResponse))
                return Ok(result);
            else
                return BadRequest(result);
        }

        [HttpPost("Test")]
        [ApiExplorerSettings(IgnoreApi = false)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(BillPaymentNotificationResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorResponse))]
        public ActionResult Test([FromBody]SendBillPaymentNotificationRequest req)
        {
            var result = service.SendBillPaymentTransactionIntegrated(req);
            if (result.GetType() == typeof(BillPaymentNotificationResponse))
                return Ok(result);
            else
                return BadRequest(result);
        }
    }
}
