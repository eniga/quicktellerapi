﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using QuickTellerApi.Models;
using QuickTellerApi.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace QuickTellerApi.Controllers
{
    [Route("api/[controller]")]
    public class PaymentItemController : Controller
    {
        PaymentItemService service;

        public PaymentItemController(IConfiguration configuration, ILogger<PaymentItemService> log)
        {
            service = new PaymentItemService(configuration, log);
        }

        // GET: api/values
        [HttpGet]
        public IEnumerable<PaymentItem> List()
        {
            return service.List();
        }

        // GET api/values/5
        [HttpGet("{PaymentItemId}/{BillerId}")]
        public PaymentItem Get(string PaymentItemId, string BillerId)
        {
            return service.GetById(PaymentItemId, BillerId);
        }

        [HttpGet("ByBiller/{BillerId}")]
        public IEnumerable<PaymentItem> GetByBillerId(string BillerId)
        {
            return service.GetByBillerId(BillerId);
        }
    }
}
