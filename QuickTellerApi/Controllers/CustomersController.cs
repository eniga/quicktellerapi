﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using QuickTellerApi.Models;
using QuickTellerApi.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace QuickTellerApi.Controllers
{
    [Route("api/[controller]")]
    public class CustomersController : Controller
    {
        CustomerValidationService service;

        public CustomersController(IConfiguration configuration, ILogger<QrResponseService> log)
        {
            service = new CustomerValidationService(configuration, log);
        }

        [HttpPost("validation")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CustomerValidationResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorResponse))]
        public ActionResult ValidateCustomer([FromBody]CustomerValidationRequest req)
        {
            var result = service.GetCustomerValidation(req);
            if (result.GetType() == typeof(CustomerValidationResponse))
                return Ok(result);
            else
                return BadRequest(result);
        }
    }
}
