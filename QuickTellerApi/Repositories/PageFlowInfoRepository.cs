﻿using System;
using System.Data;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using QuickTellerApi.Models;

namespace QuickTellerApi.Repositories
{
    public class PageFlowInfoRepository : DapperRepository<PageFlowInfo>
    {
        public PageFlowInfoRepository(IDbConnection connection, ISqlGenerator<PageFlowInfo> sqlGenerator)
            : base(connection, sqlGenerator)
        {
        }
    }
}
