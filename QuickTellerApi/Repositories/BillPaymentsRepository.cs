﻿using System;
using System.Data;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using QuickTellerApi.Models;

namespace QuickTellerApi.Repositories
{
    public class BillPaymentsRepository : DapperRepository<SendBillPaymentDb>
    {
        public BillPaymentsRepository(IDbConnection connection, ISqlGenerator<SendBillPaymentDb> sqlGenerator)
            : base(connection, sqlGenerator)
        {
        }
    }
}
