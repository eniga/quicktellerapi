﻿using System;
using System.Data;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using QuickTellerApi.Models;

namespace QuickTellerApi.Repositories
{
    public class PaymentItemRepository : DapperRepository<PaymentItem>
    {
        public PaymentItemRepository(IDbConnection connection, ISqlGenerator<PaymentItem> sqlGenerator)
            : base(connection, sqlGenerator)
        {
        }
    }
}
