﻿using System;
using System.Data;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using QuickTellerApi.Models;

namespace QuickTellerApi.Repositories
{
    public class BillerRepository : DapperRepository<Biller>
    {
        public BillerRepository(IDbConnection connection, ISqlGenerator<Biller> sqlGenerator)
            : base(connection, sqlGenerator)
        {
        }
    }
}
