﻿using System;
using System.Data;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using QuickTellerApi.Models;

namespace QuickTellerApi.Repositories
{
    public class CategoryRepository : DapperRepository<Category>
    {
        public CategoryRepository(IDbConnection connection, ISqlGenerator<Category> sqlGenerator)
            : base(connection, sqlGenerator)
        {
        }
    }
}
