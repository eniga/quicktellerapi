﻿using System;
using System.Data;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using QuickTellerApi.Models;

namespace QuickTellerApi.Repositories
{
    public class QtResponseRepository : DapperRepository<QtResponse>
    {
        public QtResponseRepository(IDbConnection connection, ISqlGenerator<QtResponse> sqlGenerator)
            : base(connection, sqlGenerator)
        {
        }
    }
}
