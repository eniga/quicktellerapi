﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using QuickTellerApi.Models;
using QuickTellerApi.Repositories;

namespace QuickTellerApi.Services
{
    public class CategoryService
    {
        CategoryRepository repo;
        readonly ILogger<CategoryService> logger;

        public CategoryService(IConfiguration configuration, ILogger<CategoryService> log)
        {
            var conn = new SqlConnection(configuration.GetConnectionString("DefaultConnection"));
            var generator = new SqlGenerator<Category>(SqlProvider.MSSQL);
            repo = new CategoryRepository(conn, generator);
            logger = log;
        }

        public IEnumerable<Category> List()
        {
            IEnumerable<Category> result;
            try
            {
                result = repo.FindAll();
            }
            catch (Exception ex)
            {
                result = null;
                logger.LogError(ex.Message);
            }
            return result;
        }

        public Category GetById(string categoryid)
        {
            Category result;
            try
            {
                result = repo.Find(x => x.categoryid == categoryid);
            }
            catch (Exception ex)
            {
                result = null;
                logger.LogError(ex.Message);
            }
            return result;
        }
    }
}
