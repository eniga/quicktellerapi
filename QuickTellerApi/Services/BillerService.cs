﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using QuickTellerApi.Models;
using QuickTellerApi.Repositories;

namespace QuickTellerApi.Services
{
    public class BillerService
    {
        BillerRepository repo;
        readonly ILogger<BillerService> logger;

        public BillerService(IConfiguration configuration, ILogger<BillerService> log)
        {
            var conn = new SqlConnection(configuration.GetConnectionString("DefaultConnection"));
            var generator = new SqlGenerator<Biller>(SqlProvider.MSSQL);
            repo = new BillerRepository(conn, generator);
            logger = log;
        }

        public IEnumerable<Biller> List()
        {
            IEnumerable<Biller> result;
            try
            {
                result = repo.FindAll();
            }
            catch (Exception ex)
            {
                result = null;
                logger.LogError(ex.Message);
            }
            return result;
        }

        public Biller GetById(string Id)
        {
            Biller result;
            try
            {
                result = repo.Find(x => x.billerid == Id);
            }
            catch (Exception ex)
            {
                result = null;
                logger.LogError(ex.Message);
            }
            return result;
        }

        public IEnumerable<Biller> GetByCategory(string CategoryId)
        {
            IEnumerable<Biller> result;
            try
            {
                result = repo.FindAll(x => x.categoryid == CategoryId);
            }
            catch (Exception ex)
            {
                result = null;
                logger.LogError(ex.Message);
            }
            return result;
        }
    }
}
