﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using QuickTellerApi.Models;
using QuickTellerApi.Repositories;

namespace QuickTellerApi.Services
{
    public class PageFlowInfoService
    {
        PageFlowInfoRepository repo;
        readonly ILogger<PageFlowInfoService> logger;

        public PageFlowInfoService(IConfiguration configuration, ILogger<PageFlowInfoService> log)
        {
            var conn = new SqlConnection(configuration.GetConnectionString("DefaultConnection"));
            var generator = new SqlGenerator<PageFlowInfo>(SqlProvider.MSSQL);
            repo = new PageFlowInfoRepository(conn, generator);
            logger = log;
        }

        public IEnumerable<PageFlowInfo> List()
        {
            IEnumerable<PageFlowInfo> result;
            try
            {
                result = repo.FindAll();
            }
            catch (Exception ex)
            {
                result = null;
                logger.LogError(ex.Message);
            }
            return result;
        }

        public PageFlowInfo GetById(string BillerId)
        {
            PageFlowInfo result;
            try
            {
                result = repo.Find(x => x.billerid == BillerId);
            }
            catch (Exception ex)
            {
                result = null;
                logger.LogError(ex.Message);
            }
            return result;
        }
    }
}
