﻿using System;
using System.Net;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using QuickTellerApi.Models;
using QuickTellerApi.Utilities;
using RestSharp;

namespace QuickTellerApi.Services
{
    public class CustomerValidationService
    {
        IConfiguration config;
        QrResponseService qrResponseService;
        readonly ILogger<QrResponseService> logger;
        string TerminalID = string.Empty;

        public CustomerValidationService(IConfiguration configuration, ILogger<QrResponseService> log)
        {
            config = configuration;
            qrResponseService = new QrResponseService(configuration, log);
            TerminalID = configuration.GetValue<string>("QuickTeller:TerminalID");
        }

        public Object GetCustomerValidation(CustomerValidationRequest req)
        {
            Object response = new Object();
            string baseUrl = config.GetValue<String>("QuickTeller:baseUrl");
            string ApiUrl = $"api/v2/quickteller/customers/validations";

            RestClient client = new RestClient(baseUrl);
            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            var header = HeaderUtility.GenerateAPIHeader("GET", baseUrl + ApiUrl, "SHA1", config);

            var request = new RestRequest(ApiUrl, method: Method.POST);

            request.AddHeader("Authorization", "InterswitchAuth " + header.Authorization);
            request.AddHeader("Signature", header.Signature);
            request.AddHeader("SignatureMethod", header.SignatureMethod);
            request.AddHeader("Timestamp", header.Timestamp);
            request.AddHeader("TerminalID", header.TerminalID);
            request.AddHeader("Nonce", header.Nonce);

            request.AddJsonBody(req);

            var result = client.Execute(request);
            if (result.IsSuccessful)
            {
                response = JsonConvert.DeserializeObject<CustomerValidationResponse>(result.Content);
            }
            else
            {
                response = JsonConvert.DeserializeObject<ErrorResponse>(result.Content);
            }

            return response;
        }
    }
}
