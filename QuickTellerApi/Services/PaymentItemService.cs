﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using QuickTellerApi.Models;
using QuickTellerApi.Repositories;

namespace QuickTellerApi.Services
{
    public class PaymentItemService
    {
        PaymentItemRepository repo;
        readonly ILogger<PaymentItemService> logger;

        public PaymentItemService(IConfiguration configuration, ILogger<PaymentItemService> log)
        {
            var conn = new SqlConnection(configuration.GetConnectionString("DefaultConnection"));
            var generator = new SqlGenerator<PaymentItem>(SqlProvider.MSSQL);
            repo = new PaymentItemRepository(conn, generator);
            logger = log;
        }

        public IEnumerable<PaymentItem> List()
        {
            IEnumerable<PaymentItem> result;
            try
            {
                result = repo.FindAll();
            }
            catch (Exception ex)
            {
                result = null;
                logger.LogError(ex.Message);
            }
            return result;
        }

        public PaymentItem GetById(string PaymentItemId, string BillerId)
        {
            PaymentItem result;
            try
            {
                result = repo.Find(x => x.paymentitemid == PaymentItemId && x.billerid == BillerId);
            }
            catch (Exception ex)
            {
                result = null;
                logger.LogError(ex.Message);
            }
            return result;
        }

        public IEnumerable<PaymentItem> GetByBillerId(string BillerId)
        {
            IEnumerable<PaymentItem> result;
            try
            {
                result = repo.FindAll(x => x.billerid == BillerId);
            }
            catch (Exception ex)
            {
                result = null;
                logger.LogError(ex.Message);
            }
            return result;
        }

        public PaymentItem GetByPaymentCode(string PaymentCode)
        {
            PaymentItem result = new PaymentItem();
            try
            {
                result = repo.Find(x => x.paymentCode == PaymentCode);
            }
            catch (Exception ex)
            {
                result = null;
                logger.LogError(ex.Message);
            }
            return result;
        }
    }
}
