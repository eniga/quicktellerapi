﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Net;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using QuickTellerApi.Models;
using QuickTellerApi.Models.FlexConnect;
using QuickTellerApi.Repositories;
using QuickTellerApi.Utilities;
using RestSharp;

namespace QuickTellerApi.Services
{
    public class SendBillPaymentService
    {
        IConfiguration config;
        QrResponseService qrResponseService;
        readonly ILogger<QrResponseService> logger;
        FlexConnectService flexConnectService;
        string TerminalID = string.Empty;
        BillPaymentsRepository repo;

        public SendBillPaymentService(IConfiguration configuration, ILogger<QrResponseService> log)
        {
            config = configuration;
            qrResponseService = new QrResponseService(configuration, log);
            flexConnectService = new FlexConnectService(configuration, log);
            TerminalID = configuration.GetValue<string>("QuickTeller:TerminalID");
            var conn = new SqlConnection(configuration.GetConnectionString("DefaultConnection"));
            var generator = new SqlGenerator<SendBillPaymentDb>(SqlProvider.MSSQL);
            repo = new BillPaymentsRepository(conn, generator);
        }

        public Object SendBillPaymentTransaction(SendBillPaymentNotificationPayload req)
        {
            Object response = new Object();
            req.Amount = req.Amount * 100;
            var dbreq = new SendBillPaymentDb()
            {
                amount = req.Amount.ToString(),
                customerId = req.CustomerId,
                paymentCode = req.PaymentCode,
                requestReference = req.requestReference,
                accountnumber = req.AccountNumber,
                TerminalId = TerminalID
            };
            try
            {
                
                repo.Insert(dbreq);
                // Check Amount
                if (req.Amount < 1)
                {
                    response = new ErrorResponse()
                    {
                        error = new Error()
                        {
                            code = "90013",
                            message = "Invalid Amount"
                        },
                        errors = new List<Error>() { new Error() { code = "90013", message = "Invalid Amount" } }
                    };
                    dbreq.responseCode = "90013";
                    dbreq.responseMessage = "Invalid Amount";
                    dbreq.responseCodeGrouping = dbreq.responseCode;
                    repo.Update(dbreq);
                    return response;
                }
                
                // Debit Customer before calling Interswitch Bill Payment service
                string accountnumber = req.AccountNumber;

                // Get flexconnect config
                var section = config.GetSection("FlexConnectConfig");
                FlexConnectConfig flexConnectConfig = section.Get<FlexConnectConfig>();

                //FlexConnectConfig flexConnectConfig =  config.GetValue<FlexConnectConfig>("FlexConnectConfig");
                // Name enquiry
                FlexAccount account = flexConnectService.GetBalance(accountnumber);
                if (account == null)
                {
                    response = new ErrorResponse()
                    {
                        error = new Error()
                        {
                            code = "90052",
                            message = "Invalid Account"
                        },
                        errors = new List<Error>() { new Error() { code = "90051", message = "Insufficient Funds" } }
                    };
                    dbreq.responseCode = "90052";
                    dbreq.responseMessage = "Invalid Account";
                    dbreq.responseCodeGrouping = dbreq.responseCode;
                    repo.Update(dbreq);
                    return response;
                }

                dbreq.customerEmail = account.EmailAdd;
                dbreq.customerMobile = account.PhoneNo;
                repo.Update(dbreq);

                // Defining Transaction body
                FundTransferRequestBody transferRequestBody = new FundTransferRequestBody()
                {
                    amount = req.Amount,
                    Channel = flexConnectConfig.Channel,
                    CreditAccount = flexConnectConfig.CreditAccount,
                    CurrencyCode = flexConnectConfig.CurrencyCode,
                    isCharge = flexConnectConfig.isCharge.ToString(),
                    Narration = flexConnectConfig.Narration.Replace("[CustomerName]", account.CustomerName),
                    SourceCode = flexConnectConfig.SourceCode,
                    TransCode = flexConnectConfig.TransCode,
                    TransRef = req.requestReference,
                    TrnDate = DateTime.Now,
                    UserID = flexConnectConfig.UserID,
                    ValueDate = DateTime.Now,
                    DebitAccount = account.CustAccNo
                };
                FundTransferRequest fundTransferRequest = new FundTransferRequest()
                {
                    Body = transferRequestBody
                };

                // Pass Debit
                var debitResponse = flexConnectService.FundTransfer(fundTransferRequest);
                if (debitResponse.ResponseCode == "00")
                {
                    SendBillPaymentNotificationRequest request = new SendBillPaymentNotificationRequest()
                    {
                        amount = req.Amount.ToString(),
                        customerEmail = account.EmailAdd,
                        customerId = req.CustomerId,
                        customerMobile = account.PhoneNo,
                        paymentCode = req.PaymentCode,
                        requestReference = req.requestReference,
                        TerminalId = TerminalID
                    };
                    // Call Interswitch Bill Payment Service
                    var result = SendBillPaymentTransactionIntegrated(request);
                    if (result.GetType() == typeof(ErrorResponse))
                    {
                        ErrorResponse res = (ErrorResponse)result;
                        // Pass reversal
                        fundTransferRequest.Body.amount = decimal.Negate(fundTransferRequest.Body.amount);
                        flexConnectService.FundTransfer(fundTransferRequest);
                        dbreq.responseCode = res.error.code;
                        dbreq.responseMessage = res.error.message;
                        dbreq.responseCodeGrouping = dbreq.responseCode;
                        repo.Update(dbreq);
                    }
                    else
                    {
                        BillPaymentNotificationResponse res = (BillPaymentNotificationResponse)result;
                        dbreq.responseCode = res.responseCode;
                        dbreq.responseMessage = res.responseMessage;
                        dbreq.responseCodeGrouping = res.responseCodeGrouping;
                        dbreq.transactionRef = res.transactionRef;
                        repo.Update(dbreq);
                    }
                }
                string log = Helper.ObjectToJson(response);
                logger.LogDebug("QuickTeller:: ProcessPayment returns {0}", log);
            }
            catch (Exception ex)
            {
                response = new ErrorResponse()
                {
                    error = new Error()
                    {
                        code = "E26",
                        message = "System Malfunction"
                    }
                };
                dbreq.responseCode = "E26";
                dbreq.responseMessage = "System Malfunction";
                dbreq.responseCodeGrouping = dbreq.responseCode;
                repo.Update(dbreq);
                logger.LogError(ex.Message);
            }
            return response;
        }

        public Object SendBillPaymentTransactionIntegrated(SendBillPaymentNotificationRequest req)
        {
            Object response = new Object();
            string baseUrl = config.GetValue<String>("QuickTeller:baseUrl");
            string ApiUrl = $"api/v2/quickteller/payments/advices";

            RestClient client = new RestClient(baseUrl);
            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            var header = HeaderUtility.GenerateAPIHeader("GET", baseUrl + ApiUrl, "SHA1", config);

            var request = new RestRequest(ApiUrl, method: Method.POST);

            request.AddHeader("Authorization", "InterswitchAuth " + header.Authorization);
            request.AddHeader("Signature", header.Signature);
            request.AddHeader("SignatureMethod", header.SignatureMethod);
            request.AddHeader("Timestamp", header.Timestamp);
            request.AddHeader("TerminalID", header.TerminalID);
            request.AddHeader("Nonce", header.Nonce);

            req.requestReference = header.RequestReferencePrefix + req.requestReference;
            req.amount = (int.Parse(req.amount) * 100).ToString();
            request.AddJsonBody(req);

            var result = client.Execute(request);
            if (result.IsSuccessful)
            {
                response = JsonConvert.DeserializeObject<BillPaymentNotificationResponse>(result.Content);
            }
            else
            {
                response = JsonConvert.DeserializeObject<ErrorResponse>(result.Content);
            }

            return response;
        }

        public Object QueryTransaction(string RequestReference)
        {
            Object response = new Object();
            RequestReference = config.GetValue<String>("QuickTeller:RequestReferencePrefix") + RequestReference;
            string baseUrl = config.GetValue<String>("QuickTeller:baseUrl");
            string ApiUrl = $"api/v2/quickteller/transactions?requestreference={RequestReference}";

            RestClient client = new RestClient(baseUrl);
            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            var header = HeaderUtility.GenerateAPIHeader("GET", baseUrl + ApiUrl, "SHA1", config);

            var request = new RestRequest(ApiUrl, method: Method.GET);

            request.AddHeader("Authorization", "InterswitchAuth " + header.Authorization);
            request.AddHeader("Signature", header.Signature);
            request.AddHeader("SignatureMethod", header.SignatureMethod);
            request.AddHeader("Timestamp", header.Timestamp);
            request.AddHeader("TerminalID", header.TerminalID);
            request.AddHeader("Nonce", header.Nonce);

            var result = client.Execute<TransactionDetails>(request);
            if (result.IsSuccessful)
            {
                var qtResp = qrResponseService.GetByIdAsync(result.Data.transactionResponseCode).Result;
                response = new BillPaymentNotificationResponse()
                {
                    responseCode = qtResp.Code,
                    responseCodeGrouping = null,
                    responseMessage = qtResp.Description,
                    transactionRef = result.Data.transactionRef
                };
            }
            else
            {
                response = JsonConvert.DeserializeObject<ErrorResponse>(result.Content);
            }

            return response;
        }

    }
}
