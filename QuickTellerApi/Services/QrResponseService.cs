﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using QuickTellerApi.Models;
using QuickTellerApi.Repositories;

namespace QuickTellerApi.Services
{
    public class QrResponseService
    {
        QtResponseRepository repo;
        readonly ILogger<QrResponseService> logger;

        public QrResponseService(IConfiguration configuration, ILogger<QrResponseService> log)
        {
            var conn = new SqlConnection(configuration.GetConnectionString("DefaultConnection"));
            var generator = new SqlGenerator<QtResponse>(SqlProvider.MSSQL);
            repo = new QtResponseRepository(conn, generator);
            logger = log;
        }

        public async Task<IEnumerable<QtResponse>> ListAsync()
        {
            IEnumerable<QtResponse> result;
            try
            {
                result = await repo.FindAllAsync();
            }
            catch (Exception ex)
            {
                result = null;
                logger.LogError(ex.Message);
            }
            return result;
        }

        public async Task<QtResponse> GetByIdAsync(string Code)
        {
            QtResponse result;
            try
            {
                result = await repo.FindAsync(x => x.Code == Code);
            }
            catch (Exception ex)
            {
                result = null;
                logger.LogError(ex.Message);
            }
            return result;
        }
    }
}
